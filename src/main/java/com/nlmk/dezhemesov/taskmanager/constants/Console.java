package com.nlmk.dezhemesov.taskmanager.constants;

/**
 * Константные значения для использования в консольном модуле
 */
public class Console {
    /**
     * Сведения об авторе
     */
    public static final String KEY_ABOUT = "about";
    /**
     * Помощь по ключам запуска
     */
    public static final String KEY_HELP = "help";
    /**
     * Сведения о версии
     */
    public static final String KEY_VERSION = "version";
}
